# Using nfs-cilent

## Prerequisite
1. Running Kubernetes Cluster
1. Deploy NFS Server and make sure all node that use NFS can connect to NFS Server

## Walkthrough
> please note that any argument in format of <name> will be the same throughout this file.

1. Setup RBAC for nfs-client in Kubernetes cluster
```
kubectl create -n kube-system -f auth/clusterrole.yaml
kubectl create -n kube-system -f auth/clusterrolebinding.yaml
kubectl create -n kube-system -f auth/serviceaccount.yaml
```

2. Configure nfs-client deployment in `deployment.yaml`
```
...
env:
  - name: PROVISIONER_NAME
    value: <provisioner-name>
  - name: NFS_SERVER
    value: <nfs-server-address>
  - name: NFS_PATH
    value: <nfs-path>
volumes:
- name: nfs-client-root
nfs:
  server: <nfs-server-address>
  path: <nfs-path>
...
```

3. Configure nfs-client storageClass in `class.yaml`
```
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: <storage-class-name>
provisioner: <provisioner-name> # must match deployment's env PROVISIONER_NAME
```

4. Deploy nfs-cliet stack
```
kubectl create -n kube-system -f deployment.yaml
kubectl create -n kube-system -f class.yaml
# Make sure deployment pod is up
kubectl get po -n kube-system
```

## Verification
1. Configure persistent volume claim from NFS Server
```
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: <claim-name>
  annotations:
    volume.beta.kubernetes.io/storage-class: "<storage-class-name>"
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Mi
```

2. Configure test pod yaml file
```
...
volumeMounts:
  - name: nfs-pvc
    mountPath: "/mnt"
restartPolicy: "Never"
volumes:
- name: nfs-pvc
  persistentVolumeClaim:
    claimName: <claim-name>
...
```

3. Test claim persistent volume from NFS Server
```
kubectl create -f test-claim.yaml
# Make sure pvc is created and already bind volume
kubectl get pvc
# Make sure pv exists
kubectl get pv
```

4. Test create pod to use persistent volume
```
kubectl create -f test-pod.yaml
# Make sure pod is created
kubectl get po
```

5. Verify file named "SUCCESS" at the nfs path in NFS server
6. Cleanup Kubernetes
```
kubectl delete -f test-pod.yaml
kubectl delete -f test-claim.yaml
```

7. Cleanup files in NFS Server
